FROM pyjam/yate-docker:latest
EXPOSE 5060/udp

# ffmpeg needed for audio conversion
RUN apt-get update
RUN apt-get install ffmpeg -y

# python deps
COPY ./requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

# more python deps
RUN pip install python-yate
RUN pip install regex
RUN pip install numpy==1.16.4
RUN pip install tensorflow==1.13.0rc1
RUN pip install tqdm
RUN pip install pyyaml==4.2b1
RUN pip install requests


COPY ./AI-DungeonMaster/ /scripts/

COPY conf.d /yate-SVN/conf.d

CMD ./run -vv -CDon
