run: build
	docker-compose up 

build:
	docker-compose build

clean: 
	docker rm -f sip-adventure 

submod:
	git submodule update --init --recursive
	cd AI-DungeonMaster/gpt2 && python download_model.py 117M
