#!/usr/bin/env python3

"""
Yate module, records sound for up to 15 seconds
and then plays the sound file back to the caller.
"""

# std lib
import os
import asyncio
import random

# deps
from yate.ivr import YateIVR


async def main(ivr: YateIVR):
    # make a quick beep, so the handset knows there's audio to play
    await ivr.tone("busy")
    await asyncio.sleep(1)
    await ivr.silence()

    with open('/tmp/fuck', 'w') as f:
        f.write(str(ivr.call_params))

    # make random filename
    recording_filename = str(random.randint(1000, 9999)) + ".slin"
    recording_path = os.path.join("/tmp/", recording_filename)

    # start recording audio - this call is non-blocking
    t = await ivr.record_audio(recording_path, time_limit_s=15)

    # this call is blocking
    await ivr.read_dtmf_symbols(1, timeout_s=15)
    await ivr.tone("busy")

    # stop recording
    if not t.done():
        t.cancel()
        await ivr.stop_recording()

    # play back audio
    await ivr.play_soundfile(recording_path, complete=True)


ivr = YateIVR()
ivr.run(main)
